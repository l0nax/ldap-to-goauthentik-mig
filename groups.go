package main

import (
	"context"
	"fmt"
	"log/slog"
	"strings"

	"github.com/go-ldap/ldap/v3"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"go.l0nax.org/typact"
	"goauthentik.io/api/v3"
)

func syncGroups(c *cli.Context) error {
	createOnly := c.Bool("create-only")

	if err := LoadConfig(c.String("config")); err != nil {
		return err
	}

	slog.Debug("Connecting to LDAP server", slog.String("ldap_host", C.LDAP.Host))

	server, err := ldap.DialURL(C.LDAP.Host)
	if err != nil {
		return errors.Wrap(err, "unable to connect to LDAP server")
	}
	defer server.Close()

	slog.Debug("Connected with LDAP Server. Start binding with user",
		slog.String("bind_user", C.LDAP.BindUser))

	err = server.Bind(C.LDAP.BindUser, C.LDAP.BindPassword)
	if err != nil {
		return errors.Wrap(err, "unable to bind with user")
	}

	groupTree := fmt.Sprintf("%s,%s", C.LDAP.GroupDN, C.LDAP.BaseDN)
	filter := C.LDAP.GroupFilter.UnwrapOr("(objectclass=groupofnames)")
	attributes := []string{
		C.Attributes.GroupName,
		C.Attributes.GroupDescription,
	}

	slog.Debug("Start searching for groups",
		slog.String("group_tree", groupTree), slog.String("filter", filter),
		slog.Any("attributes", attributes))

	req := ldap.NewSearchRequest(
		groupTree,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases,
		0,
		0,
		false,
		filter,
		attributes,
		nil,
	)

	search, err := server.Search(req)
	if err != nil {
		return errors.Wrap(err, "unable to search for groups")
	}

	cli := createGoAuthentikClient()

	for _, entry := range search.Entries {
		groupName := entry.GetAttributeValue(C.Attributes.GroupName)
		groupDN := entry.DN
		desc := entry.GetAttributeValue(C.Attributes.GroupDescription)

		slog.Debug("Processing group",
			slog.String("group_name", groupName), slog.String("group_dn", groupDN),
			slog.String("group_description", desc))

		if err = createGroup(createOnly, cli, groupName, desc); err != nil {
			return err
		}
	}

	slog.Info("All groups created!")

	return nil
}

func createGroup(createOnly bool, cli *api.APIClient, name, desc string) error {
	create := true
	groupUUID := typact.None[string]()
	if !createOnly {
		list, _, err := cli.CoreApi.CoreGroupsList(getAuthentikCtx()).
			Name(name).
			Execute()
		if err != nil {
			return errors.Wrap(err, "unable to create group")
		}

		res := list.GetResults()
		if len(res) > 0 {
			create = false
			groupUUID = typact.Some(res[0].GetPk())
		}
	}

	if create {
		resp, _, err := cli.CoreApi.CoreGroupsCreate(getAuthentikCtx()).
			GroupRequest(api.GroupRequest{
				Name: name,
			}).
			Execute()
		if err != nil {
			return errors.Wrap(err, "unable to create group")
		}

		slog.Debug("Group created on GoAuthentik side!", slog.Any("group_id", resp.Pk))
	} else {
		attrs := make(map[string]interface{})
		if desc != "" {
			attrs["notes"] = desc
		}

		resp, _, err := cli.CoreApi.CoreGroupsUpdate(getAuthentikCtx(), groupUUID.Unwrap()).
			GroupRequest(api.GroupRequest{
				Name:       name,
				Attributes: attrs,
			}).
			Execute()
		if err != nil {
			return errors.Wrap(err, "unable to create group")
		}

		slog.Debug("Group updated on GoAuthentik side!", slog.Any("group_id", resp.Pk))
	}

	return nil
}

type LDAPUserEntry struct {
	DN string `ldap:"dn"`

	// memberOf may have multiple values. If you don't
	// know the amount of attribute values at runtime, use a string array.
	MemberOf []string `ldap:"memberOf"`
}

func syncMembers(c *cli.Context) error {
	if err := LoadConfig(c.String("config")); err != nil {
		return err
	}

	slog.Debug("Connecting to LDAP server", slog.String("ldap_host", C.LDAP.Host))

	server, err := ldap.DialURL(C.LDAP.Host)
	if err != nil {
		return errors.Wrap(err, "unable to connect to LDAP server")
	}
	defer server.Close()

	slog.Debug("Connected with LDAP Server. Start binding with user",
		slog.String("bind_user", C.LDAP.BindUser))

	err = server.Bind(C.LDAP.BindUser, C.LDAP.BindPassword)
	if err != nil {
		return errors.Wrap(err, "unable to bind with user")
	}

	userTree := fmt.Sprintf("%s,%s", C.LDAP.UserDN, C.LDAP.BaseDN)
	filter := C.LDAP.UserFilter.UnwrapOr("(objectclass=inetorgperson)")
	attributes := []string{
		C.Attributes.Username,
		C.Attributes.MemberOf,
	}

	slog.Debug("Start searching for users",
		slog.String("user_tree", userTree), slog.String("filter", filter),
		slog.Any("attributes", attributes))

	req := ldap.NewSearchRequest(
		userTree,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases,
		0,
		0,
		false,
		filter,
		attributes,
		nil,
	)

	search, err := server.Search(req)
	if err != nil {
		return errors.Wrap(err, "unable to search for users")
	}

	slog.Info("Server returned user entries", slog.Int("number_entries", len(search.Entries)))

	cli := createGoAuthentikClient()
	helper := newGroupAssigner(c.Bool("skip-unknown-groups"), cli)

	for _, entry := range search.Entries {
		username := entry.GetAttributeValue(C.Attributes.Username)
		userEntry := LDAPUserEntry{}

		if err = entry.Unmarshal(&userEntry); err != nil {
			return errors.Wrap(err, "unable to marshal user entry")
		}

		slog.Debug(
			"Syncing user",
			slog.String("username", username),
			slog.Any("ldap_user_entry", userEntry),
		)

		err = helper.assignUserToGroups(username, userEntry.MemberOf)
		if err != nil {
			return errors.Wrap(err, "unable to create user")
		}

		slog.Debug("Assigned user to groups", slog.String("username", username))
	}

	slog.Info("All users successfully created!")

	return nil
}

type groupAssigner struct {
	groupToUUID       map[string]string
	cli               *api.APIClient
	ctx               context.Context
	skipUnknownGroups bool
}

func newGroupAssigner(skipUnknownGroups bool, cli *api.APIClient) *groupAssigner {
	return &groupAssigner{
		cli:               cli,
		groupToUUID:       make(map[string]string, 15),
		ctx:               getAuthentikCtx(),
		skipUnknownGroups: skipUnknownGroups,
	}
}

func (g *groupAssigner) getGroupUUIDByName(name string) (string, error, bool) {
	uuid, ok := g.groupToUUID[name]
	if ok {
		return uuid, nil, false
	}

	resp, _, err := g.cli.CoreApi.
		CoreGroupsList(g.ctx).
		Name(name).
		Execute()
	if err != nil {
		return "", errors.Wrapf(err, "unable to find group by name %q", name), false
	}

	results := resp.GetResults()
	if len(results) == 0 {
		return "", fmt.Errorf("group not found: no results returned for %q", name), g.skipUnknownGroups
	}

	uuid = results[0].GetPk()
	g.groupToUUID[name] = uuid

	slog.Debug("Found group UUID by name", slog.String("name", name), slog.String("group_id", uuid))

	return uuid, nil, false
}

func (g *groupAssigner) assignUserToGroups(username string, groups []string) error {
	slog.Debug("Get User ID by username", slog.String("username", username))

	resp, _, err := g.cli.CoreApi.
		CoreUsersList(g.ctx).
		Username(username).
		Execute()
	if err != nil {
		return errors.Wrap(err, "unable to fetch user info")
	}

	userRes := resp.GetResults()
	if len(userRes) == 0 {
		return fmt.Errorf("user %q not found: no results returned", username)
	}

	userID := userRes[0].GetPk()
	slog.Debug("Retrieved user ID", slog.String("username", username), slog.Any("user_id", userID))

	for _, group := range groups {
		groupName := g.extractGroupName(group)

		gid, err, skip := g.getGroupUUIDByName(groupName)
		if err != nil {
			if skip {
				slog.Warn("Skipping group assignment", slog.String("user", username), slog.String("group", groupName))
				continue
			}

			return errors.Wrap(err, "unable to fetch group info")
		}

		_, err = g.cli.CoreApi.
			CoreGroupsAddUserCreate(g.ctx, gid).
			UserAccountRequest(api.UserAccountRequest{
				Pk: userID,
			}).
			Execute()
		if err != nil {
			return errors.Wrap(err, "unable to assign user to group")
		}

		slog.Debug("Assigned user to group")
	}

	return nil
}

func (*groupAssigner) extractGroupName(entry string) string {
	idx := strings.Index(entry, ",")
	if idx < 0 {
		panic(fmt.Sprintf("unable to extract group name based on memberOf value from %q", entry))
	}

	entry = entry[:idx]

	idx = strings.Index(entry, "=")
	if idx < 0 {
		panic(fmt.Sprintf("unable to extract group name based on memberOf value from %q", entry))
	}

	return entry[idx+1:]
}

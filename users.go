package main

import (
	"fmt"
	"log/slog"

	"github.com/go-ldap/ldap/v3"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"goauthentik.io/api/v3"
)

func syncUsers(c *cli.Context) error {
	dryRun := c.Bool("dry-run")

	if err := LoadConfig(c.String("config")); err != nil {
		return err
	}

	slog.Debug("Connecting to LDAP server", slog.String("ldap_host", C.LDAP.Host))

	server, err := ldap.DialURL(C.LDAP.Host)
	if err != nil {
		return errors.Wrap(err, "unable to connect to LDAP server")
	}
	defer server.Close()

	slog.Debug("Connected with LDAP Server. Start binding with user",
		slog.String("bind_user", C.LDAP.BindUser))

	err = server.Bind(C.LDAP.BindUser, C.LDAP.BindPassword)
	if err != nil {
		return errors.Wrap(err, "unable to bind with user")
	}

	userTree := fmt.Sprintf("%s,%s", C.LDAP.UserDN, C.LDAP.BaseDN)
	filter := C.LDAP.UserFilter.UnwrapOr("(objectclass=inetorgperson)")
	attributes := []string{
		C.Attributes.Username,
		C.Attributes.Mail,
		C.Attributes.DisplayName,
	}

	slog.Debug("Start searching for users",
		slog.String("user_tree", userTree), slog.String("filter", filter),
		slog.Any("attributes", attributes))

	req := ldap.NewSearchRequest(
		userTree,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases,
		0,
		0,
		false,
		filter,
		attributes,
		nil,
	)

	search, err := server.Search(req)
	if err != nil {
		return errors.Wrap(err, "unable to search for users")
	}

	slog.Info("Server returned user entries", slog.Int("number_entries", len(search.Entries)))

	cli := createGoAuthentikClient()

	for _, entry := range search.Entries {
		username := entry.GetAttributeValue(C.Attributes.Username)
		email := entry.GetAttributeValue(C.Attributes.Mail)
		displayName := entry.GetAttributeValue(C.Attributes.DisplayName)

		slog.Debug("Syncing user", slog.String("username", username), slog.String("email", email))

		err = createUser(dryRun, cli, UserInfo{
			Username: username,
			Email:    email,
			FullName: displayName,
		})
		if err != nil {
			return errors.Wrap(err, "unable to create user")
		}

		slog.Debug("Created user", slog.String("username", username))
	}

	slog.Info("All users successfully created!")

	return nil
}

type UserInfo struct {
	Username string
	Email    string
	FullName string
}

func createUser(dryRun bool, cli *api.APIClient, user UserInfo) error {
	if dryRun {
		slog.Info("Skipping user creation on goauthentik because of dry-run", slog.Any("user_info", user))

		return nil
	}

	resp, _, err := cli.CoreApi.CoreUsersCreate(getAuthentikCtx()).
		UserRequest(api.UserRequest{
			Username: user.Username,
			Name:     user.FullName,
			IsActive: toPtr(true),
			Email:    toPtr(user.Email),
		}).
		Execute()
	if err != nil {
		return errors.Wrap(err, "unable to create user")
	}

	slog.Debug("User created on GoAuthentik side!", slog.Any("user_id", resp.Pk))

	return nil
}

func toPtr[T any](val T) *T {
	return &val
}

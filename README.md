# LDAP to GoAuthentik migrator

This is a simple CLI I've written to automatically migrate 
* Users
* Groups
* Group memberships

from LDAP to GoAuthentik.

The config struct in [`config.go`](./config.go) documents how to configure the migrator.

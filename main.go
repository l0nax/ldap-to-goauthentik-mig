package main

import (
	"fmt"
	"log/slog"
	"os"
	"strings"
	"time"

	"github.com/lmittmann/tint"
	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name:  "lagauth-mig",
		Usage: "LDAP to GoAuthentik Migrator",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "config",
				Usage:    "File path to the config file.",
				Required: true,
			},
			&cli.StringFlag{
				Name:  "log-level",
				Usage: "Defines the log level. Allowed values: debug, info, warn, error.",
				Value: "info",
			},
			&cli.BoolFlag{
				Name:  "dry-run",
				Usage: "Skips modifications on the GoAuthentik side.",
				Value: false,
			},
			&cli.BoolFlag{
				Name:  "create-only",
				Usage: "If set to true and a user/ group already exists, the process will fail",
				Value: false,
			},

			&cli.BoolFlag{
				Name:  "skip-unknown-groups",
				Usage: "Instead of throwing an error, the migrator skips an unknown group",
				Value: false,
			},
		},
		Action: mainAction,
		Before: setupLogger,
		Commands: []*cli.Command{
			{
				Name:   "sync-users",
				Usage:  "Synchronizes users from LDAP to GoAuthentik.",
				Action: syncUsers,
			},
			{
				Name:   "sync-groups",
				Usage:  "Synchronizes groups from LDAP to GoAuthentik.",
				Action: syncGroups,
			},
			{
				Name:   "sync-member-mapping",
				Usage:  "Synchronizes the group membership from LDAP to GoAuthentik.",
				Action: syncMembers,
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		slog.Error("FATAL", slog.Any("error", err))
		os.Exit(1)
	}
}

func mainAction(c *cli.Context) error {
	return cli.ShowAppHelp(c)
}

func setupLogger(c *cli.Context) error {
	var lvl slog.Level
	switch strings.ToLower(c.String("log-level")) {
	case "debug":
		lvl = slog.LevelDebug
	case "info":
		lvl = slog.LevelInfo
	case "warn":
		lvl = slog.LevelWarn
	case "error":
		lvl = slog.LevelError
	default:
		return fmt.Errorf("unknown log level %q", c.String("log-level"))
	}

	slog.SetDefault(slog.New(
		tint.NewHandler(os.Stdout, &tint.Options{
			Level:      lvl,
			TimeFormat: time.RFC3339,
		}),
	))

	return nil
}

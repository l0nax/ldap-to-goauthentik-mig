package main

import (
	"context"

	"goauthentik.io/api/v3"
)

func createGoAuthentikClient() *api.APIClient {
	cfg := &api.Configuration{
		DefaultHeader: make(map[string]string),
		UserAgent:     "OpenAPI-Generator/1.0.0/go",
		Debug:         false,
		Servers: api.ServerConfigurations{
			{
				URL:         C.GoAuthentik.Endpoint + "/api/v3",
				Description: "No description provided",
			},
		},
		OperationServers: map[string]api.ServerConfigurations{},
	}

	return api.NewAPIClient(cfg)
}

func getAuthentikCtx() context.Context {
	ctx := context.Background()

	return context.WithValue(ctx, api.ContextAPIKeys, map[string]api.APIKey{
		"authentik": {
			Key:    C.GoAuthentik.APIKey,
			Prefix: "Bearer",
		},
	})
}

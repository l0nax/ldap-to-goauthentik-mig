package main

import (
	"fmt"

	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/file"
	"github.com/knadh/koanf/v2"
	"go.l0nax.org/typact"
)

type FilterConfig struct {
	Exclude []string `koanf:"exclude"`
}

type Config struct {
	LDAP struct {
		BindUser     string `koanf:"bind_user"`
		BindPassword string `koanf:"bind_password"`
		// Host is the LDAP URI.
		//
		// Example: ldap://my.server.de
		// Example: ldapi://%2fdata%2frun%2fslapd-localhost.socket
		Host string `koanf:"host"`

		BaseDN string `koanf:"base_dn"`
		// UserDN is the DN for the users.
		//
		// Example: cn=users,cn=accounts
		UserDN string `koanf:"user_dn"`
		// GroupDN is the DN for the groups.
		//
		// Example: cn=groups,cn=accounts
		GroupDN string `koanf:"group_dn"`

		// UserFilter is the filter used to search for users.
		//
		// Default: (objectclass=inetorgperson)
		UserFilter  typact.Option[string] `koanf:"user_filter"`
		GroupFilter typact.Option[string] `koanf:"group_filter"`
	} `koanf:"ldap"`

	// Attributes configures the attribute mapping from LDAP to GoAuthentik.
	Attributes struct {
		Username    string `koanf:"username"`
		Mail        string `koanf:"mail"`
		DisplayName string `koanf:"display_name"`
		MemberOf    string `koanf:"member_of"`

		// GroupName is the attribute holding the name of the group.
		//
		// Example: cn
		GroupName        string `koanf:"group_name"`
		GroupDescription string `koanf:"group_description"`
	} `koanf:"attributes"`

	// Filter allows to filter users and groups.
	Filter struct {
	} `koanf:"filter"`

	GoAuthentik struct {
		Endpoint string `koanf:"endpoint"`
		APIKey   string `koanf:"api_key"`
	} `koanf:"go_authentik"`
}

var C Config

func LoadConfig(path string) error {
	k := koanf.New(".")

	if err := k.Load(file.Provider(path), yaml.Parser()); err != nil {
		return err
	}

	if err := k.Unmarshal("", &C); err != nil {
		return err
	}

	return assertMany(
		assertNonEmpty(C.Attributes.Username, "C.Attributes.Username"),
		assertNonEmpty(C.Attributes.DisplayName, "C.Attributes.DisplayName"),
		assertNonEmpty(C.Attributes.Mail, "C.Attributes.Mail"),
		assertNonEmpty(C.Attributes.MemberOf, "C.Attributes.MemberOf"),
		assertNonEmpty(C.Attributes.GroupName, "C.Attributes.GroupName"),
	)
}

func assertMany(errs ...error) error {
	for _, err := range errs {
		if err != nil {
			return err
		}
	}

	return nil
}

func assertNonEmpty(val, name string) error {
	if val != "" {
		return nil
	}

	return fmt.Errorf("expected value %q to be non empty", name)
}
